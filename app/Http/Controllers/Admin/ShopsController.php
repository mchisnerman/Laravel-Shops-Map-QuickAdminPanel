<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Day;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyShopRequest;
use App\Http\Requests\StoreShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Shop;
use Gate;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ShopsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('shop_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(auth::user()->is_admin){
            $shops = Shop::all();    
        }else{
            $shops = Shop::where('created_by_id', auth::user()->id)
                            ->get();    
        }
        

        return view('admin.shops.index', compact('shops'));
    }

    public function create()
    {
        abort_if(Gate::denies('shop_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id');
        $days = Day::all();

        return view('admin.shops.create', compact('categories', 'days'));
    }

    public function store(StoreShopRequest $request)
    {
        $inputs = $request->all();
        if($request->hasFile("logo")){
            $file1=$request->file("logo");
            
            $nombre1 = "logo_".time().".".$file1->guessExtension();

            $ruta = public_path('images').'/'.$nombre1;

            if($file1->guessExtension()=="png" or $file1->guessExtension()=="jpg" or $file1->guessExtension()=="jpeg" or $file1->guessExtension()=="bmp"){
                copy($file1, $ruta);
            }else{
                return redirect()->back()
            ->with('error','No ha subido una imagen válida. Solo se aceptan PNG, JPG, o BMP');
                
            }
        
        $inputs['logo'] = $nombre1;


        }
        
        $shop = Shop::create($inputs);
        $shop->categories()->sync($request->input('categories', []));

        $hours = collect($request->input('from_hours'))->mapWithKeys(function($value, $id) use ($request) {
            return $value ? [ 
                    $id => [
                        'from_hours'    => $value, 
                        'from_minutes'  => $request->input('from_minutes.'.$id), 
                        'to_hours'      => $request->input('to_hours.'.$id),
                        'to_minutes'    => $request->input('to_minutes.'.$id)
                    ]
                ] 
                : [];
        });
        $shop->days()->sync($hours);

        foreach ($request->input('photos', []) as $file) {
            $shop->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photos');
        }

        return redirect()->route('admin.shops.index');
    }

    public function edit(Shop $shop)
    {
        abort_if(Gate::denies('shop_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id');
        $days = Day::all();

        $shop->load('categories', 'created_by', 'days');

        return view('admin.shops.edit', compact('categories', 'shop', 'days'));
    }

    public function update(UpdateShopRequest $request, Shop $shop)
    {
        if(!$request->active){
            $request->merge([
                'active' => 0
            ]);
        }
        $inputs = $request->all();
        if($request->hasFile("logo")){
            $file1=$request->file("logo");
            
            $nombre1 = "logo_".time().".".$file1->guessExtension();

            $ruta = public_path('images').'/'.$nombre1;

            if($file1->guessExtension()=="png" or $file1->guessExtension()=="jpg" or $file1->guessExtension()=="jpeg" or $file1->guessExtension()=="bmp"){
                copy($file1, $ruta);
            }else{
                return redirect()->back()
            ->with('error','No ha subido una imagen válida. Solo se aceptan PNG, JPG, o BMP');
                
            }
        
        $inputs['logo'] = $nombre1;

        }
        
       
        $shop->update($inputs);
        $shop->categories()->sync($request->input('categories', []));

        $hours = collect($request->input('from_hours'))->mapWithKeys(function($value, $id) use ($request) {
            return $value ? [ 
                    $id => [
                        'from_hours'    => $value, 
                        'from_minutes'  => $request->input('from_minutes.'.$id), 
                        'to_hours'      => $request->input('to_hours.'.$id),
                        'to_minutes'    => $request->input('to_minutes.'.$id)
                    ]
                ] 
                : [];
        });
        $shop->days()->sync($hours);

        if (count($shop->photos) > 0) {
            foreach ($shop->photos as $media) {
                if (!in_array($media->file_name, $request->input('photos', []))) {
                    $media->delete();
                }
            }
        }

        $media = $shop->photos->pluck('file_name')->toArray();

        foreach ($request->input('photos', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $shop->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photos');
            }
        }

        return redirect()->route('admin.shops.index');
    }

    public function show(Shop $shop)
    {
        abort_if(Gate::denies('shop_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $days = Day::all();
        $shop->load('categories', 'created_by');

        return view('admin.shops.show', compact('shop', 'days'));
    }

    public function destroy(Shop $shop)
    {
        abort_if(Gate::denies('shop_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $shop->delete();

        return back();
    }

    public function massDestroy(MassDestroyShopRequest $request)
    {
        Shop::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
