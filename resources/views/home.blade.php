@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box content-single">
                <article class="post-8 page type-page status-publish hentry">
                    <header>
                        <h1 class="entry-title">{{ request()->filled('search') || request()->filled('category') || request()->filled('localidad') ? 'Search results' : 'Todos' }}</h1></header>
                    <div class="entry-content entry-summary">
                        <div class="geodir-search-container geodir-advance-search-default" data-show-adv="default">
                            <form class="geodir-listing-search gd-search-bar-style" name="geodir-listing-search" action="{{ route('home') }}" method="get">
                                <div class="geodir-loc-bar">
                                    <div class="clearfix geodir-loc-bar-in">
                                        <div class="geodir-search">
                                            <div class='gd-search-input-wrapper gd-search-field-cpt gd-search-field-taxonomy gd-search-field-categories'>
                                                <select name="category" class="cat_select">
                                                    <option value="">Categoria</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"{{ old('category', request()->input('category')) == $category->id ? ' selected' : '' }}>{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class='gd-search-input-wrapper gd-search-field-cpt gd-search-field-taxonomy gd-search-field-localidad'>
                                            <select name="localidad" class="loc_select">
                                                <option value="">Localidad</option>
                                                <option value="San Isidro">San Isidro</option>
                                                <option value="Martinez">Martinez</option>
                                            </select>
                                            </div>
                                            <div class='gd-search-input-wrapper gd-search-field-search'> <span class="geodir-search-input-label"><i class="fas fa-search gd-show"></i><i class="fas fa-times geodir-search-input-label-clear gd-hide" title="Clear field"></i></span>
                                                <input class="search_text gd_search_text" name="search" value="{{ old('search', request()->input('search')) }}" type="text" placeholder="Buscar..." aria-label="Buscar..." autocomplete="off" />
                                            </div>
                                            <button class="geodir_submit_search" data-title="fas fa-search" aria-label="fas fa-search"><i class="fas fas fa-search" aria-hidden="true"></i><span class="sr-only">Buscar</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="geodir-loop-container">
                            <ul class="geodir-category-list-view clearfix gridview_onethird geodir-listing-posts geodir-gridview gridview_onethird">
                                @foreach($shops as $shop)
                                    <li class="gd_place type-gd_place status-publish has-post-thumbnail">
                                        <div class="gd-list-item-left ">
                                            <div class="geodir-post-slider">
                                                <div class="geodir-image-container geodir-image-sizes-medium_large">
                                                    <div class="geodir-image-wrapper">
                                                        <ul class="geodir-post-image geodir-images clearfix">
                                                            <li>
                                                                <a href='{{ route('shop', $shop->id) }}'>
                                                                    <img src="{{ $shop->thumbnail }}" width="1440" height="960" class="geodir-lazy-load align size-medium_large" />
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gd-list-item-right ">
                                            <div class="geodir-post-title">
                                                <h2 class="geodir-entry-title"> <a href="{{ route('shop', $shop->id) }}" title="Ver: {{ $shop->name }}">{{ $shop->name }}</a></h2></div>
                                            @foreach($shop->categories as $category)
                                                <div class="gd-badge-meta gd-badge-alignleft" title="{{ $category->name }}">
                                                    <div class="gd-badge" style="background-color:#ffb100;color:#ffffff;"><i class="fas fa-certificate"></i> <span class='gd-secondary'>{{ $category->name }}</span></div>
                                                </div>
                                            @endforeach
                                            @if($shop->days->count())
                                                <div class="geodir-post-meta-container">
                                                    <div class="geodir_post_meta gd-bh-show-field gd-lv-s-2 geodir-field-business_hours gd-bh-toggled gd-bh-{{ $shop->working_hours->isOpen() ? 'open' : 'close' }}" style="clear:both;">
                                                        <span class="geodir-i-business_hours geodir-i-biz-hours">
                                                            <i class="fas fa-clock" aria-hidden="true"></i><font>{{ $shop->working_hours->isOpen() ? 'Abierto' : 'Cerrado' }} ahora</font>
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="geodir-post-content-container">
                                                <div class="geodir_post_meta  geodir-field-post_content" style='max-height:120px;overflow:hidden;'>{!! $shop->description !!} <a href='{{ route('shop', $shop->id) }}' class='gd-read-more  gd-read-more-fade'>Leer más...</a></div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="clear"></div>
                        </div>
                        {{ $shops->appends(request()->query())->links('partials.pagination') }}
                    </div>
                    <footer class="entry-footer"></footer>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{{-- <script type='text/javascript' src='https://maps.google.com/maps/api/js?language=en&key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&region=GB'></script> --}}
<script>
	
if(!$("#latitud").val()){
    latitud=-34.4696078;
}else{
    latitud=$("#latitud").val();
}


if(!$("#longitud").val()){
    longitud=-58.512917;
}else{
    longitud=$("#longitud").val();
}


L.mapbox.accessToken = 'pk.eyJ1IjoibWNoaXNuZXJtYW4iLCJhIjoiY2psNWRrMHRsMmxzZzNxcWs5ZHd4b2s0dyJ9.YXcrwOu_j6P4FE9nRf1qvw';
var map = L.map('map-canvas').setView([latitud, longitud], 18);
// Capas base
L.tileLayer(
    'https://api.mapbox.com/styles/v1/mchisnerman/ckrpabpr12ina17pdg8jxo0hn/tiles/{z}/{x}/{y}?access_token=' + L.mapbox.accessToken, {
        tileSize: 512,
        zoomOffset: -1,
        attribution: '© <a href="https://apps.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);


@foreach($shops as $shop)
var myIcon = L.icon({
    iconUrl: "{{asset('images/'.$shop->logo)}}",
    iconSize: [96, 96],
    //shadowUrl: 'my-icon-shadow.png',
    //shadowSize: [68, 95],
    //shadowAnchor: [22, 94]
});
var marker = L.marker([{{$shop->latitude}}, {{$shop->longitude}}],{icon: myIcon}).addTo(map);

@endforeach
    
</script>
@endsection