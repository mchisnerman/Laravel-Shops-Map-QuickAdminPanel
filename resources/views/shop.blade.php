@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box content-single">
                <article class="post-180 gd_place type-gd_place status-publish hentry gd_placecategory-hotels">
                    <header>
                        <h1 class="entry-title">{{ $shop->name }}</h1></header>
                    <div class="entry-content entry-summary">
                        @if($shop->photos->count())
                            <div class="geodir-post-slider center-gallery">
                                <div class="bxslider">
                                    @foreach($shop->photos as $photo)
                                    <div><img src="{{ $photo->url }}"></div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if($shop->categories->count())
                            <div class="geodir-single-taxonomies-container">
                                <p class="geodir_post_taxomomies clearfix"> 
                                    <span class="geodir-category">
                                        Categorías: 
                                        @foreach($shop->categories as $category)
                                            <a href="{{ route('home') }}?category={{ $category->id }}">{{ $category->name }}</a>{{ !$loop->last ? ',' : ''  }}
                                        @endforeach
                                    </span>
                                </p>
                            </div>
                        @endif
                        <div class="geodir-single-tabs-container">
                            <div class="geodir-tabs" id="gd-tabs">
                                <dl class="geodir-tab-head"><dt></dt>
                                    <dd class="geodir-tab-active"><a data-tab="#post_content" data-status="enable"><i class="fas fa-home" aria-hidden="true"></i>Info</a></dd><dt></dt>
                                    @if($shop->photos->count())
                                        <dd class=""><a data-tab="#post_images" data-status="enable"><i class="fas fa-image" aria-hidden="true"></i>Imágenes</a></dd><dt></dt>
                                    @endif
                                    @if($shop->latitude && $shop->longitude)
                                        <dd class=""><a data-tab="#post_map" data-status="enable"><i class="fas fa-globe-americas" aria-hidden="true"></i>Mapa</a></dd><dt></dt>
                                    @endif
                                    @if($shop->days->count())
                                        <dd class=""><a data-tab="#working_hours" data-status="enable"><i class="fas fa-clock" aria-hidden="true"></i>Dias y Horarios</a></dd>
                                    @endif
                                </dl>
                                <ul class="geodir-tabs-content geodir-entry-content " style="z-index: 0; position: relative;">
                                    <li id="post_contentTab" style="display: none;"><span id="post_content"></span>
                                        <div id="geodir-tab-content-post_content" class="hash-offset"></div>
                                        <div class="geodir-post-meta-container">
                                            <div class="geodir_post_meta  geodir-field-post_content">
                                                <p>Dirección: {{ $shop->address }}</p>
                                                <p>Localidad: {{ $shop->localidad }}</p>
                                                <p>Descripción: {!! $shop->description !!}</p>
                                                
                                                <p></p>
                                            </div>
                                        </div>
                                    </li>
                                    @if($shop->photos->count())
                                        <li id="post_imagesTab" style="display: none;"><span id="post_images"></span>
                                            <div id="geodir-tab-content-post_images" class="hash-offset"></div>
                                            <div class="geodir-post-slider">
                                                <div class="geodir-image-container geodir-image-sizes-medium_large ">
                                                    <div id="geodir_images_5de6cafacbba5_180" class="geodir-image-wrapper" data-controlnav="1" data-slideshow="1">
                                                        <ul class="geodir-gallery geodir-images clearfix">
                                                            @foreach($shop->photos as $photo)
                                                                <li>
                                                                    <a href="{{ $photo->getUrl() }}" class="geodir-lightbox-image" target="_blank"><img src="{{ $photo->getUrl('thumb') }}" width="1440" height="960"><i class="fas fa-search-plus" aria-hidden="true"></i></a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    @if($shop->latitude && $shop->longitude)
                                        <li id="post_mapTab" style="display: none;">
                                            <div id="map-canvas" style="height: 425px; width: 100%; position: relative; overflow: hidden;">
                                            </div>
                                        </li>
                                    @endif
                                    @if($shop->days->count())
                                        <li id="working_hoursTab" style="display: none;">
                                            @foreach($shop->days as $day)
                                                <p>{{ ucfirst($day->name) }}: de {{ $day->pivot->from_hours }}:{{ $day->pivot->from_minutes }} a {{ $day->pivot->to_hours }}:{{ $day->pivot->to_minutes }}</p>
                                            @endforeach
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="geodir-single-taxonomies-container">
                            <div class="geodir-pos_navigation clearfix">
                                <div class="geodir-post_left">
                                    <a href="{{ url()->previous() }}" rel="prev">Volver</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="entry-footer"></footer>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<style>
@media only screen and (min-width: 675px) {
    .center-gallery {
        width: 50%;
        margin: auto;
    }
}
</style>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script type="text/javascript">
if (window.location.hash && window.location.hash.indexOf('&') === -1 && jQuery(window.location.hash + 'Tab').length) {
    hashVal = window.location.hash;
} else {
    hashVal = jQuery('dl.geodir-tab-head dd.geodir-tab-active').find('a').attr('data-tab');
}
openTab(hashVal);

jQuery('dl.geodir-tab-head dd a').click(function() {
    openTab(jQuery(this).data('tab'))
});

function openTab(hashVal)
{
    
    jQuery('dl.geodir-tab-head dd').each(function() {
        var tab = '';
        tab = jQuery(this).find('a').attr('data-tab');
        jQuery(this).removeClass('geodir-tab-active');
        if (hashVal != tab) {
            jQuery(tab + 'Tab').hide();
        }
    });
    jQuery('a[data-tab="'+hashVal+'"]').parent().addClass('geodir-tab-active');
    jQuery(hashVal + 'Tab').show();
if (hashVal=="#post_map"){
    map.invalidateSize();
}

}

$(function(){
    $('.bxslider').bxSlider({
        mode: 'fade',
        slideWidth: 600
    });
});
</script>
@if($shop->latitude && $shop->longitude)
   <script>
        L.mapbox.accessToken = 'pk.eyJ1IjoibWNoaXNuZXJtYW4iLCJhIjoiY2psNWRrMHRsMmxzZzNxcWs5ZHd4b2s0dyJ9.YXcrwOu_j6P4FE9nRf1qvw';
        var map = L.map('map-canvas').setView([{{$shop->latitude}}, {{$shop->longitude}}], 18);
        // Capas base
        L.tileLayer(
            'https://api.mapbox.com/styles/v1/mchisnerman/ckrpabpr12ina17pdg8jxo0hn/tiles/{z}/{x}/{y}?access_token=' + L.mapbox.accessToken, {
                tileSize: 512,
                zoomOffset: -1,
                attribution: '© <a href="https://apps.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
        var myIcon = L.icon({
            iconUrl: "{{asset('images/'.$shop->logo)}}",
            iconSize: [96, 96],
            //shadowUrl: 'my-icon-shadow.png',
            //shadowSize: [68, 95],
            //shadowAnchor: [22, 94]
        });
        var marker = L.marker([{{$shop->latitude}}, {{$shop->longitude}}],{icon: myIcon}).addTo(map);
   </script>
@endif
@endsection