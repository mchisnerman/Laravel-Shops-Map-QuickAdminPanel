<?php

return [
    'failed'   => 'Estas credenciales no corresponden a ningún usuario.',
    'throttle' => 'Demasiados intentos. Por favor pruebe mas tarde',
];
