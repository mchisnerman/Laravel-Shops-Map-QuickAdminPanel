<?php

return [
    'password' => 'La contraseña debe contener al menos 6 caracteres y ser iguales.',
    'reset'    => 'Su contraseña ha sido reseteada',
    'sent'     => 'Le hemos enviado el mail para resetear la contraseña!',
    'token'    => 'El token es inválido.',
    'user'     => 'No podemos encontrar un usuario con ese mail.',
    'updated'  => 'Su contraseña se ha cambiado con éxito!',
];
